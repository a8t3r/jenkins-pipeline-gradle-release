#!/usr/bin/env groovy​

import org.jenkinsci.plugins.pipeline.modeldefinition.Utils

// enable 'Branch Sources -> Advanced clone behaviours -> Fetch tags'
def call(Map pipelineParams) {

    def definedParameters = [
            [$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', numToKeepStr: '5']],
            pipelineTriggers([pollSCM('H/5 * * * *')])
    ]

    if (env.BRANCH_NAME == 'develop') {
        definedParameters.add(parameters([choice(
                choices: ['no', 'yes'],
                description: 'Parameter choice with default value \'no\' for manual release initialization',
                name: 'perform_release')]))
    }

    properties(definedParameters)

    stage('prepare') {
        node {
            checkout scm
            working_branch = "${env.BRANCH_NAME}"
            working_version = version()
            version_tag = getVersionTag(working_branch)
            currentBuild.displayName = "${working_branch}-${working_version} build ${env.BUILD_NUMBER}"

            sh 'git config user.name jenkins'
            sh 'git config user.email jenkins@unknownways'
        }
    }

    node {
        try {
            stage('test') {
                gradle "clean test"
            }
        } finally {
            junit 'build/test-results/**/*.xml'
        }
    }

    stage('build docker image') {
        when(version_tag != null && params.perform_release != 'yes') {
            node {
                gradle("dockerPushImage -PtagVersion=$version_tag")
            }
        }
    }

    stage('init release') {
        when(working_branch == 'develop' && params.perform_release == 'yes') {
            node {
                def tmpAskPassScript = pwd(tmp: true) + "/bitbucket-cloud"
                def askPassScript = """
                    #!/bin/sh
                    case "\$1" in
                    Username*) echo \$USERNAME ;;
                    Password*) echo \$PASSWORD ;;
                    esac
                """

                writeFile(file: tmpAskPassScript, text: askPassScript)
                withCredentials([usernamePassword(credentialsId: pipelineParams.git_credentials_id, passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
                    sh "chmod +x ${tmpAskPassScript}"

                    withEnv(["GIT_ASKPASS=${tmpAskPassScript}"]) {
                        sh "git config remote.origin.fetch '+refs/heads/*:refs/remotes/origin/*'"
                        sh "git fetch origin master"
                        gradle("release -Prelease.useAutomaticVersion=true")
                    }
                }
            }
        }
    }

    stage('deploy') {
        when(version_tag != null && params.perform_release != 'yes' ) {
            def message = "Starting deployment for branch ${working_branch} with version tag ${version_tag}"
            echo message
        }
    }
}

def getVersionTag(String branch) {
    if (branch == 'develop') {
        def lastCommit = sh script: 'git rev-parse HEAD', returnStdout: true
        return lastCommit.substring(0, 7)
    } else {
        def lastTag = sh script: 'git describe --exact-match --abbrev=0 --tags 2> /dev/null || echo empty', returnStdout: true
        return lastTag =~ 'empty' ? null : lastTag
    }
}

def gradle(String goals) {
    def javaHome = tool "Java"

    withEnv(["JAVA_HOME=${javaHome}"]) {
        // https://stackoverflow.com/questions/37171043/gradle-build-daemon-disappeared-unexpectedly-it-may-have-been-killed-or-may-hav
        sh "${env.WORKSPACE}/gradlew --no-daemon ${goals}"
    }
}

def version() {
    def matcher = readFile('gradle.properties') =~ 'version=(.*)'
    return matcher ? matcher[0][1] : null
}

def when(boolean condition, body) {
    def config = [:]
    body.resolveStrategy = Closure.OWNER_FIRST
    body.delegate = config

    if (condition) {
        body()
    } else {
        Utils.markStageSkippedForConditional(STAGE_NAME)
    }
}